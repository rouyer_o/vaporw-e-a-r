const path = require('path');
const UglifiJSPlugin = require('uglifyjs-webpack-plugin')
const dev = process.env.NODE_ENV === "dev"


let config = {
    entry: './assets/js/app.js',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },
    watch: dev,
    devtool : dev ? "cheap-module-eval-source-map" : "source-map",
    module : {
        rules : [
            {
                test : /\.js$/,
                exclude:/(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test : /\.css$/,
                use : [
                    'style-loader',
                    'css-loader'
                ]
            },
            {
                test : /\.(png | svg | jpg | gif)$/,
                use : [
                    'file-loader'
                ]
            }
        ]
    },
    plugins:[
    ],
    mode : 'development'
};
if(!dev){
  config.plugins.push(new UglifiJSPlugin({
    sourceMap: true
  }))
}

module.exports = config
